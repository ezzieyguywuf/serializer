/*
    OccWrapper, a C++ wrapper for OpenCascade.
    Copyright (C) 2018  Wolfgang E. Sanyer. See LICENSE file for more info.
*/
#include "gtest/gtest.h"

#include <sstream>

TEST(Serializer, serializeInt)
{
    Serialize::Serializer out("SENTINEL");
    out << 10 << 20 << 30;
}
